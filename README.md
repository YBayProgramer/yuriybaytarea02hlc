## Ejercicio 1
Para reproducir sonido utilizo MediaPleyer
```sh
    private MediaPlayer mp;
    mp = MediaPlayer.create(this, R.raw.alarma);
    public void reproducirSonido() {
        mp.start();
    }
```
Para guardar el fichero mp3 he creado una carpeta que se llama 'raw'

Para limitar el número de cafés he añadido condición 'if' que verifica el número de cafés actuales a la hora de pulsar el botón 'Comenzar'. En caso cuando el usuario ha llegado al límite 10 cafés, sale una ventana con el mensaje informativo e impide comenzar la cuenta atrás o cronometro.
```sh
if (view == binding.botonComenzar) {
        if (!binding.cuenta.getText().equals("10")) {
            if (descendente == true) {
                contadorDescendente = new MyCountDownTimer(contador.obtenerTiempo() * 60 * 1000, 1000); //multiplico porque necesito milisegundo y tendo minutos
                contadorDescendente.start();
                estadoBotones(false);
            } else {
                startTimerUp();
            }
        } else {
            AlertDialog.Builder popup = new AlertDialog.Builder(this);
            popup.setTitle("Limite de cafes");
            popup.setMessage("Has llegado al limite de cafes: 10");
            popup.setPositiveButton("Salir", null);
            popup.show();
        }
    }
```

Para poder continuar la utilización del programa he añadido un botón 'Restablecer' que pone a cero el resultado de contador de cafés e informa el usuario sobre la acción.
```sh
if (view == binding.botonRestablecer) {
        binding.cuenta.setText(contador.restablecerContadorCafes());
        AlertDialog.Builder popup = new AlertDialog.Builder(this);
        popup.setTitle("Restablecer");
        popup.setMessage("El numero de cafes anulado.");
        popup.setPositiveButton("Salir", null);
        popup.show();
    }
```

Como mejora a la aplicación he añadido 'Switch' para poder elegir la cuenta descendente o ascendente del cronómetro.
```sh
private void checkSwitch() {
        binding.switchModo.setOnCheckedChangeListener((compoundButton, b) -> descendente = b == true ? false : true);
    }
```
Al pulsar el switch se cambia el valor del boolean 'descendente' que luego utilizo para que el programa ejecuta el contador correcto.
```sh
if (descendente == true) {
        contadorDescendente = new MyCountDownTimer(contador.obtenerTiempo() * 60 * 1000, 1000); //multiplico porque necesito milisegundo y tendo minutos
        contadorDescendente.start();
        estadoBotones(false);
    } else {
        startTimerUp();
    }
```

Para el contador ascedente utilizo Timer y TimerTask.
```sh
Timer timer;
TimerTask timerTask;
Double time = 0.0;
timer = new Timer();
    public void startTimerUp() {
        estadoBotones(false);
        timerTask = new TimerTask() {
            String maximo = (binding.tiempo.length() == 4) ? "0" + binding.tiempo.getText().toString() : binding.tiempo.getText().toString();
            String tiempoActual = "0:00";

            @Override
            public void run() {
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        if (!maximo.equals(tiempoActual)) {
                            time++;
                            binding.tiempo.setText(getTimerText());
                            tiempoActual = binding.tiempo.getText().toString();
                        } else {
                            System.out.println("FIN");
                            reproducirSonido();
                            binding.cuenta.setText(contador.aumentarCafes());
                            time = Double.valueOf(0);
                            estadoBotones(true);
                            timerTask.cancel();
                        }
                    }
                });
            }
        };
        timer.scheduleAtFixedRate(timerTask, 0, 1000);
    }

public String getTimerText() {
    int rounded = (int) Math.round(time);
    int seconds = ((rounded % 86400) % 3600) % 60;
    int minuts = ((rounded % 86400) % 3600) / 60;

    return formatTimer(minuts, seconds);
}

private String formatTimer(int minuts, int seconds) {
    return String.format("%02d", minuts) + ":" + String.format("%02d", seconds);
}
```
## Ejercicio 2
Para poder abrir el URL en el explorador web, antes de todo hay que dar los permisos al acceso a internet a 'AndroidManifest.xml'
```sh 
  <uses-permission android:name="android.permission.INTERNET" />
```
Ahora puedo abrir sin problemas una URL existente en el explorador y para esto utilizo el método creado que utiliza 'Intent':
```sh
public void openThisUrlInBrowser(View view, String url) {
    Intent intent = new Intent(Intent.ACTION_VIEW);
    intent.setData(Uri.parse(url));
    startActivity(intent);
}
```
Para poder abrir la web desde la aplicación hay que utilizar WebView.
```sh
 <WebView
    android:id="@+id/IdWebView"
    android:layout_width="409dp"
    android:layout_height="729dp"
    app:layout_constraintBottom_toBottomOf="parent"
    app:layout_constraintEnd_toEndOf="parent"
    app:layout_constraintStart_toStartOf="parent"
    app:layout_constraintTop_toTopOf="parent" />
```
Desde la MainActivity con 'Intent' y 'Bundle' al pulsar el botón cambio la actividad y paso URL desde EditText de la MainActivity a la segunda actividad:
```sh
public void moveToSeceondActivity(View view) {
        String url = binding.editTextLink.getText().toString();
        Intent intent = new Intent(MainActivity.this, SecondActivityWebView.class);
        Bundle bundle = new Bundle();
        bundle.putString("link", url);
        intent.putExtras(bundle);
        if (intent.resolveActivity(getPackageManager()) != null) {
            startActivity(intent);
        }else{
            showDialogWindow("Error", "Boton no funciona correctamente", "Aceptar");
        }
    }
```
En la segunda actividad obtengo el URL y lo pongo en WebView:
```sh
String url = getStringURL();
binding.IdWebView.loadUrl(url);

    private String getStringURL() {
        Bundle bundle = getIntent().getExtras();
        String url  = bundle.getString("link");
        return url;
    }
```

## Ejercicio 3
Para la interfaz grafica yo he utilizafo los elementos como:
- Button
- EditText

Para coger todos los números primos inferiores al número introducido por el usuario yo utilizo el bucle 'for' y verifico cada número desde cero si es primo, y en caso afirmativo añado a stringBuilder. Al final cambio el texto de editText para mostrar los numero primos.
```sh
private fun getPrimoInferior(numeroUsuario: Int, textView: TextView) {
        println("")
        var numerosPrimosInferiores = StringBuilder()
        for (x in 0..numeroUsuario) {
            if (esPrimo(x)) {
                print(x.toString() + ",")
                numerosPrimosInferiores.append(x.toString() + ", ")
                textView.text = x.toString()
            }
        }
        if (!numerosPrimosInferiores.isEmpty()){//cuando hay números primos menores que el número intorducido.
            textView.setText(numerosPrimosInferiores.toString())
        }else{
            if (numeroUsuario < 0){// si el número es negativo
                textView.setText("Número introducido es negativo: " + numeroUsuario)
            }else{//si no hay números primos menor que número introducido.
                textView.setText("No hay número primo inferior a " + numeroUsuario)
            }
        }
    }
```
EditText que yo utilizo para mostrar los números primos.
```sh
<EditText
        android:id="@+id/textViewNumerosPrimos"
        android:layout_width="319dp"
        android:layout_height="349dp"
        android:layout_marginTop="20dp"
        android:ems="10"
        android:inputType="textMultiLine"
        android:textAlignment="center"
        app:layout_constraintEnd_toEndOf="parent"
        app:layout_constraintStart_toStartOf="parent"
        app:layout_constraintTop_toBottomOf="@+id/btnBuscar" />
```
Y también lo bloqueo para no poder modificar.
```sh
editTextShowNumber.isFocusable = false
```