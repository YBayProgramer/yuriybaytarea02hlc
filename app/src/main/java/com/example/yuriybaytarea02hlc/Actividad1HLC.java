package com.example.yuriybaytarea02hlc;

import androidx.appcompat.app.AppCompatActivity;

import android.app.AlertDialog;
import android.content.ActivityNotFoundException;
import android.content.Intent;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.view.View;
import android.widget.Toast;

import com.example.yuriybaytarea02hlc.databinding.ActivityActividad1HlcBinding;

import java.util.Timer;
import java.util.TimerTask;

public class Actividad1HLC extends AppCompatActivity implements View.OnClickListener {
    private boolean descendente = true;
    private ActivityActividad1HlcBinding binding;
    private Contador contador;
    private MyCountDownTimer contadorDescendente;
    private static final int PAUSA = 3;
    private MediaPlayer mp;
    Timer timer;
    TimerTask timerTask;
    Double time = 0.0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_actividad1_hlc);
        binding = ActivityActividad1HlcBinding.inflate(getLayoutInflater());
        View view = binding.getRoot();
        setContentView(view);

        contador = new Contador(0, PAUSA);
        binding.tiempo.setText(PAUSA + ":00");
        binding.botonMenos.setOnClickListener(this);
        binding.botonMas.setOnClickListener(this);
        binding.botonComenzar.setOnClickListener(this);
        binding.botonRestablecer.setOnClickListener(this);

        mp = MediaPlayer.create(this, R.raw.alarma);

        timer = new Timer();

        checkSwitch();
    }

    @Override
    public void onClick(View view) {
        if (view == binding.botonMenos) {
            binding.tiempo.setText(contador.disminuirTiempo());
            binding.botonComenzar.setEnabled(true);
        }
        if (view == binding.botonMas) {
            binding.tiempo.setText(contador.aumentarTiempo());
            binding.botonComenzar.setEnabled(true);
        }
        if (view == binding.botonComenzar) {
            if (!binding.cuenta.getText().equals("10")) {
                if (descendente == true) {
                    contadorDescendente = new MyCountDownTimer(contador.obtenerTiempo() * 60 * 1000, 1000); //multiplico porque necesito milisegundo y tendo minutos
                    contadorDescendente.start();
                    estadoBotones(false);
                } else {
                    startTimerUp();
                }
            } else {
                alertDialog("Límite de cafés", "Has llegado al límite de cafés: 10", "Salir");
            }
        }
        if (view == binding.botonRestablecer) {
            binding.cuenta.setText(contador.restablecerContadorCafes());
            alertDialog("Restablecer", "El número de cafés anulado.", "Salir");
        }
    }

    /**
     * Método para saber que contador arrancar respecto la posición del  switch.
     */
    private void checkSwitch() {
        binding.switchModo.setOnCheckedChangeListener((compoundButton, b) -> descendente = b == true ? false : true);
    }

    /**
     * Clase responsable de la lógica de contador de la cuenta descendente (atrás).
     */
    public class MyCountDownTimer extends CountDownTimer {

        public MyCountDownTimer(long millisInFuture, long countDownInterval) {
            super(millisInFuture, countDownInterval);
        }

        @Override
        public void onTick(long l) {
            long minutos, segundos;
            minutos = (l / 1000) / 60;
            segundos = (l / 1000) % 60;

            binding.tiempo.setText(minutos + ":" + segundos);
        }

        @Override
        public void onFinish() {
            reproducirSonido();
            Toast.makeText(Actividad1HLC.this, "Pausa terminada", Toast.LENGTH_LONG).show();
            binding.cuenta.setText(contador.aumentarCafes());
            binding.tiempo.setText(contador.obtenerTiempo() + ":00");
            estadoBotones(true);
        }

    }

    /**
     * Método responsable de la lógica del contador ascendente.
     */
    public void startTimerUp() {
        estadoBotones(false);
        timerTask = new TimerTask() {
            String maximo = (binding.tiempo.length() == 4) ? "0" + binding.tiempo.getText().toString() : binding.tiempo.getText().toString();
            String tiempoActual = "0:00";

            @Override
            public void run() {
                runOnUiThread(() -> {
                    if (!maximo.equals(tiempoActual)) {
                        time++;
                        binding.tiempo.setText(getTimerText());
                        tiempoActual = binding.tiempo.getText().toString();
                    } else {
                        System.out.println("FIN");
                        reproducirSonido();
                        binding.cuenta.setText(contador.aumentarCafes());
                        time = Double.valueOf(0);
                        estadoBotones(true);
                        timerTask.cancel();
                    }
                });
            }
        };
        timer.scheduleAtFixedRate(timerTask, 0, 1000);
    }

    /**
     * Método para dar el formato a tiempo
     * @return
     */
    public String getTimerText() {
        int rounded = (int) Math.round(time);
        int seconds = ((rounded % 86400) % 3600) % 60;
        int minuts = ((rounded % 86400) % 3600) / 60;

        return formatTimer(minuts, seconds);
    }

    private String formatTimer(int minuts, int seconds) {
        return String.format("%02d", minuts) + ":" + String.format("%02d", seconds);
    }

    /**
     * Método para cambiar la disponibilidad de los botones.
     * @param enabled
     */
    public void estadoBotones(boolean enabled) {
        binding.botonMenos.setEnabled(enabled);
        binding.botonMas.setEnabled(enabled);
        binding.botonComenzar.setEnabled(enabled);
        binding.botonRestablecer.setEnabled(enabled);
    }

    /**
     * Método responsable de reproducción del sonido.
     */
    public void reproducirSonido() {
        mp.start();
    }

    /**
     * Método para llamar una ventana de diálogo con el mensaje necesario.
     * @param title
     * @param message
     * @param positiveButtonText
     */
    public void alertDialog(String title, String message, String positiveButtonText) {
        AlertDialog.Builder popup = new AlertDialog.Builder(this);
        popup.setTitle(title);
        popup.setMessage(message);
        popup.setPositiveButton(positiveButtonText, null);
        popup.show();
    }

}