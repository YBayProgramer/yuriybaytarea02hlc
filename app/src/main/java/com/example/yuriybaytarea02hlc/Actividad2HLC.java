package com.example.yuriybaytarea02hlc;

import androidx.appcompat.app.AppCompatActivity;

import android.app.AlertDialog;
import android.content.ActivityNotFoundException;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;

import com.example.yuriybaytarea02hlc.databinding.ActivityActividad2HlcBinding;

public class Actividad2HLC extends AppCompatActivity implements View.OnClickListener {
    private ActivityActividad2HlcBinding binding;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_actividad2_hlc);
        binding = ActivityActividad2HlcBinding.inflate(getLayoutInflater());
        View view = binding.getRoot();
        setContentView(view);

        binding.btnMostrarBrowser.setOnClickListener(this);
        binding.btnMostrarWebView.setOnClickListener(this);
    }

    @Override
    public void onClick(View view) {
        if (view == binding.btnMostrarBrowser){
            String url = binding.editTextLink.getText().toString();
            try{
                openThisUrlInBrowser(view, url);
            }catch (ActivityNotFoundException e){
                showDialogWindow("URL incorrecto.", "URL no encontrado.", "Aceptar");
            }
        }
        if (view == binding.btnMostrarWebView){
            moveToSeceondActivity(view);
        }
    }

    /**
     * Método para abrir el enlace en un explorador.
     * @param view
     * @param url
     */
    public void openThisUrlInBrowser(View view, String url) {
        Intent intent = new Intent(Intent.ACTION_VIEW);
        intent.setData(Uri.parse(url));
        startActivity(intent);
    }

    /**
     * Método para cambiar de actividad.
     * @param view
     */
    public void moveToSeceondActivity(View view) {
        String url = binding.editTextLink.getText().toString();
        Intent intent = new Intent(Actividad2HLC.this, SecondActivityWebView.class);
        Bundle bundle = new Bundle();
        bundle.putString("link", url);
        intent.putExtras(bundle);
        if (intent.resolveActivity(getPackageManager()) != null) {
            startActivity(intent);
        }else{
            showDialogWindow("Error", "Botón no funciona correctamente", "Aceptar");
        }
    }

    /**
     * Método para llamar una ventana de diálogo con el mensaje necesario.
     * @param title
     * @param message
     * @param positiveBotonText
     */
    public void showDialogWindow(String title, String message, String positiveBotonText) {
        AlertDialog.Builder popup = new AlertDialog.Builder(this);
        popup.setTitle(title);
        popup.setMessage(message);
        popup.setPositiveButton(positiveBotonText, null);
        popup.show();
    }
}