package com.example.yuriybaytarea02hlc

import android.app.AlertDialog
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Button
import android.widget.EditText
import android.widget.TextView

class Actividad3HLC : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_actividad3)

        lateinit var button: Button
        lateinit var editText: EditText
        lateinit var editTextShowNumber: EditText
        button = findViewById(R.id.btnBuscar);
        editText = findViewById(R.id.edittextNumero);
        editTextShowNumber = findViewById(R.id.textViewNumerosPrimos);
        editTextShowNumber.isFocusable = false

        var numeroUsuario = 0;

        button.setOnClickListener {
            try {
                numeroUsuario = editText.text.toString()
                    .toInt(); //cojo y parseo el número introducido en EditText
                getPrimoInferior(numeroUsuario, editTextShowNumber)
            } catch (e: NumberFormatException) {
                getAlertDialog("Error.", "Campo vacío.", "Aceptar")
            }
        }


    }

    /**
     * Método para llamar ventana de diálogo con el mensaje necesario.
     */
    private fun getAlertDialog(title: String, message: String, positiveButtonText: String) {
        var popup = AlertDialog.Builder(this)
        popup.setTitle(title)
        popup.setMessage(message)
        popup.setPositiveButton(positiveButtonText, null)
        popup.show()
    }

    /**
     * Método para obtener todos los números primos desde inferiores al número introducido.
     */
    private fun getPrimoInferior(numeroUsuario: Int, textView: TextView) {
        println("")
        var numerosPrimosInferiores = StringBuilder()
        for (x in 0..numeroUsuario) {
            if (esPrimo(x)) {
                print(x.toString() + ",")
                numerosPrimosInferiores.append(x.toString() + ", ")
                textView.text = x.toString()
            }
        }
        if (!numerosPrimosInferiores.isEmpty()) {//cuando hay números primos menores que el número intorducido.
            textView.setText(numerosPrimosInferiores.toString())
        } else {
            if (numeroUsuario < 0) {// si el número es negativo
                textView.setText("Número introducido es negativo: " + numeroUsuario)
            } else {//si no hay números primos menor que número introducido.
                textView.setText("No hay número primo inferior a " + numeroUsuario)
            }
        }
    }

    /**
     * Método que verifica si el número que se pasa por parámetro es primo.
     */
    fun esPrimo(numero: Int): Boolean {
        // El 0, 1 y 4 no son primos
        if (numero == 0 || numero == 1 || numero == 4) {
            return false
        }
        for (x in 2 until numero / 2) {
            // Si es divisible por cualquiera de estos números, no
            // es primo
            if (numero % x == 0) return false
        }
        // Si no se pudo dividir por ninguno de los de arriba, sí es primo
        return true
    }
}
