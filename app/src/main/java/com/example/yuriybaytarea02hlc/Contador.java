package com.example.yuriybaytarea02hlc;

public class Contador {
    private int cafes;
    private int tiempo;
    private static final int DESCANSO = 5;

    public Contador() {
        this.cafes = 0;
        this.tiempo = DESCANSO;
    }

    public Contador(int c, int t) {
        this.cafes = c;
        this.tiempo = t;
    }

    public String aumentarTiempo() {
        return String.valueOf(this.tiempo += 1) + ":00";
    }

    public String disminuirTiempo() {
        this.tiempo -= 1;
        if (this.tiempo < 1)
            tiempo = 1;
        return String.valueOf(this.tiempo) + ":00";
    }

    public String restablecerContadorCafes() {
        this.cafes = 0;
        return "0";
    }

    public int obtenerTiempo() {
        return this.tiempo;
    }

    public int obtenerCafes() {
        return this.cafes;
    }

    public String aumentarCafes() {
        this.cafes += 1;
        return String.valueOf(this.cafes);
    }
}

