package com.example.yuriybaytarea02hlc;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;

import com.example.yuriybaytarea02hlc.databinding.ActivityMainBinding;

public class MainActivity extends AppCompatActivity implements View.OnClickListener{
    private ActivityMainBinding binding;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        binding = ActivityMainBinding.inflate(getLayoutInflater());
        View view = binding.getRoot();
        setContentView(view);

        binding.btnActividad1.setOnClickListener(this);
        binding.btnActividad2.setOnClickListener(this);
        binding.btnActividad3.setOnClickListener(this);
    }

    @Override
    public void onClick(View view) {
        if (view == binding.btnActividad1){
            Intent intent = new Intent(MainActivity.this, Actividad1HLC.class);
            if (intent.resolveActivity(getPackageManager()) != null) {
                startActivity(intent);
            }
        }
        if (view == binding.btnActividad2){Intent intent = new Intent(MainActivity.this, Actividad2HLC.class);
            if (intent.resolveActivity(getPackageManager()) != null) {
                startActivity(intent);
            }
        }
        if (view == binding.btnActividad3){Intent intent = new Intent(MainActivity.this, Actividad3HLC.class);
            if (intent.resolveActivity(getPackageManager()) != null) {
                startActivity(intent);
            }
        }
    }
}