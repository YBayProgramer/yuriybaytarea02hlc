package com.example.yuriybaytarea02hlc;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;

import com.example.yuriybaytarea02hlc.databinding.ActivitySecondWebViewBinding;

public class SecondActivityWebView extends AppCompatActivity {
    private ActivitySecondWebViewBinding binding;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_second_web_view);
        binding = ActivitySecondWebViewBinding.inflate(getLayoutInflater());
        View view = binding.getRoot();
        setContentView(view);

        String url = getStringURL();

        binding.IdWebView.loadUrl(url);
    }

    /**
     * Método para obtener URL desde la MainActivity
     * @return String url
     */
    private String getStringURL() {
        Bundle bundle = getIntent().getExtras();
        String url  = bundle.getString("link");
        return url;
    }
}